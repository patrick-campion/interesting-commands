### Specific MacOS Commands

Some basic commands:
```bash
	1	Navigation and File Management:
	◦	cd: Change directory.
	◦	ls: List files and directories.
	◦	pwd: Print the working directory.
	◦	mkdir: Create a new directory.
	◦	touch: Create an empty file.
	◦	rm: Remove files and directories.
	◦	mv: Move or rename files and directories.
	◦	cp: Copy files and directories.
	2	Text Manipulation:
	◦	cat: Concatenate and display file content.
	◦	grep: Search for text using patterns.
	◦	sed: Stream editor for text manipulation.
	◦	awk: Text processing tool.
	◦	sort: Sort lines of text.
	◦	head and tail: Display the beginning or end of a file.
	3	System Information:
	◦	uname: Display system information.
	◦	top or htop: Monitor system processes and resource usage.
	◦	df and du: Display disk space usage.
	◦	ps: List running processes.
	4	Network and Connectivity:
	◦	ping: Test network connectivity.
	◦	ifconfig or ipconfig: Display network interface information.
	◦	ssh: Secure Shell remote login.
	◦	scp: Securely copy files between hosts.
	5	Package Management:
	◦	brew: The Homebrew package manager for macOS.
	◦	npm and yarn: Package managers for Node.js.
	◦	pip: Package installer for Python.
	6	File Permissions:
	◦	chmod: Change file permissions.
	◦	chown: Change file ownership.
	7	Compression and Archiving:
	◦	tar: Archive files and directories.
	◦	zip and unzip: Compress and decompress files.
	◦	gzip and gunzip: Compress and decompress files using GZIP.
	8	Disk and Filesystem Management:
	◦	diskutil: Manage disks and volumes.
	◦	fsck: Filesystem consistency check.
	9	System Control:
	◦	shutdown: Shutdown or restart the system.
	◦	sudo: Execute commands with superuser privileges.
	◦	kill: Terminate processes.
	10	Shell Customization:
	◦	nano or vim: Text editors for the Terminal.
	◦	alias: Create custom command aliases.
	◦	.bashrc or .zshrc: Shell configuration files.
```

Useful Security Commands
```bash
#System info
date #show system date
cal #show calendar
uptime #show time from starting
w #list users
whoami #this user
finger username #info about user
uname -a #sysinfo
free #check memory
df #check disk

launchctl list #List services
atq #List "at" tasks for the user
sysctl -a #List kernel configuration
diskutil list #List connected hard drives
nettop #Monitor network usage of processes in top style

system_profiler SPSoftwareDataType #System info
system_profiler SPPrintersDataType #Printer
system_profiler SPApplicationsDataType #Installed Apps
system_profiler SPFrameworksDataType #Instaled framework
system_profiler SPDeveloperToolsDataType #Developer tools info
system_profiler SPStartupItemDataType #Startup Items
system_profiler SPNetworkDataType #Network Capabilities
system_profiler SPFirewallDataType #Firewall Status
system_profiler SPNetworkLocationDataType #Known Network
system_profiler SPBluetoothDataType #Bluetooth Info
system_profiler SPEthernetDataType #Ethernet Info
system_profiler SPUSBDataType #USB info
system_profiler SPAirPortDataType #Airport Info


#Searches
mdfind password #Show all the files that contains the word password
mdfind -name password #List all the files containing the word password in the name


#Open any app
open -a <Application Name> --hide #Open app hidden
open some.doc -a TextEdit #Open a file in one application


#Computer doesn't go to sleep
caffeinate &


#Screenshot
# This will ask for permission to the user
screencapture -x /tmp/ss.jpg #Save screenshot in that file


#Get clipboard info
pbpaste


#system_profiler
system_profiler --help #This command without arguments take lot of memory and time.
system_profiler -listDataTypes
system_profiler SPSoftwareDataType SPNetworkDataType


#Network
arp -i en0 -l -a #Print the macOS device's ARP table
lsof -i -P -n | grep LISTEN
smbutil statshares -a #View smb shares mounted to the hard drive

#networksetup - set or view network options: Proxies, FW options and more
networksetup -listallnetworkservices #List network services
networksetup -listallhardwareports #Hardware ports
networksetup -getinfo Wi-Fi #Wi-Fi info
networksetup -getautoproxyurl Wi-Fi #Get proxy URL for Wifi
networksetup -getwebproxy Wi-Fi #Wifi Web proxy
networksetup -getftpproxy Wi-Fi #Wifi ftp proxy


#Brew
brew list #List installed
brew search <text> #Search package
brew info <formula>
brew install <formula>
brew uninstall <formula>
brew cleanup #Remove older versions of installed formulae.
brew cleanup <formula> #Remove older versions of specified formula.


#Make the machine talk
    say hello -v diego
#spanish: diego, Jorge, Monica
#mexican: Juan, Paulina
#french: Thomas, Amelie

########### High privileges actions
sudo purge #purge RAM
#Sharing preferences
sudo launchctl load -w /System/Library/LaunchDaemons/ssh.plist (enable ssh)
sudo launchctl unload /System/Library/LaunchDaemons/ssh.plist (disable ssh)
#Start apache
sudo apachectl (start|status|restart|stop)
 ##Web folder: /Library/WebServer/Documents/
#Remove DNS cache
dscacheutil -flushcache
sudo killall -HUP mDNSResponder
```

### Installed Software & Services

Check for **suspicious** applications installed and **privileges** over the.installed resources:

```
system_profiler SPApplicationsDataType #Installed Apps
system_profiler SPFrameworksDataType #Instaled framework
lsappinfo list #Installed Apps
launchtl list #Services
```

### User Processes

```bash
# will print all the running services under that particular user domain.
launchctl print gui/<users UID>

# will print all the running services under root
launchctl print system

# will print detailed information about the specific launch agent. And if it’s not running or you’ve mistyped, you will get some output with a non-zero exit code: Could not find service “com.company.launchagent.label” in domain for login
launchctl print gui/<user's UID>/com.company.launchagent.label
```
